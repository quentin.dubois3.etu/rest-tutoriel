package fr.ulille.iut.tva.dto;

import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InfoDetailDto {
	
	private static CalculTva CT = new CalculTva();
	
	private double montantTotal;
	private double montantTva;
	private double somme;
	private String tauxLabel;
	private double tauxValue;
	
	public InfoDetailDto() {}
	
	public InfoDetailDto(TauxTva tauxTva, double somme) {
		this.montantTotal = CT.calculerMontant(tauxTva, somme);
		this.montantTva = CT.getTaux(tauxTva);
		this.somme = somme;
		this.tauxLabel = tauxTva.name();
		this.tauxValue = somme * tauxTva.taux /100	;
	}

	public double getMontantTotal() {
		return montantTotal;
	}

	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}

	public double getMontantTva() {
		return montantTva;
	}

	public void setMontantTva(double montantTva) {
		this.montantTva = montantTva;
	}

	public double getSomme() {
		return somme;
	}

	public void setSomme(double somme) {
		this.somme = somme;
	}

	public String getTauxLabel() {
		return tauxLabel;
	}

	public void setTauxLabel(String tauxLabel) {
		this.tauxLabel = tauxLabel;
	}

	public double getTauxValue() {
		return tauxValue;
	}

	public void setTauxValue(double tauxValue) {
		this.tauxValue = tauxValue;
	}
	
	
}
